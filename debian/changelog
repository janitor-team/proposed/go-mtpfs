go-mtpfs (1.0.0-1) unstable; urgency=medium

  * New upstream release (closes: #962843, #971182)
  * Update team email address
  * Bump Standards-Version and debhelper-compat
  * Add manpage

 -- Julian Gilbey <jdg@debian.org>  Thu, 28 Jan 2021 09:21:04 +0000

go-mtpfs (0.0~git20180209.d6f8f3c-1) unstable; urgency=medium

  [ Alexandre Viau ]
  * Point Vcs-* urls to salsa.debian.org.

  [ Julian Gilbey ]
  * New upstream version 0.0~git20180209.d6f8f3c; also now following the
    Go Packaging Team numbering convention more precisely
  * Remove gen-orig-source from debian/rules as it is probably out of date
    now
  * Don't leave Go source code around unnecessarily
  * The following are based on Felix Lechner's work on
    golang-github-hanwen-go-fuse:
  * Set Standards-Version: 4.2.0 (and change Priority to optional)
  * Added debian/watch under git mode (fixed)
  * Switched to secure URI for copyright format
  * Removed Built-Using: from debian/control

 -- Julian Gilbey <jdg@debian.org>  Fri, 24 Aug 2018 00:33:36 +0100

go-mtpfs (0.0~git20150917.0.bc7c0f7-2) unstable; urgency=medium

  * Switch to XS-Go-Import-Path
  * Set Maintainer field to pkg-go, add myself to Uploaders

 -- Michael Stapelberg <stapelberg@debian.org>  Thu, 08 Feb 2018 08:58:42 +0100

go-mtpfs (0.0~git20150917.0.bc7c0f7-1) unstable; urgency=medium

  * Change build-dependency to golang-any (see bug#847476)
  * Update to latest upstream version (only a very minor change)

 -- Julian Gilbey <jdg@debian.org>  Sat, 10 Dec 2016 22:41:02 +0000

go-mtpfs (0.0~git20150802.0.3ef47f9-3) unstable; urgency=medium

  [ Paul Tagliamonte ]
  * Team upload.
  * Use a secure transport for the Vcs-Git and Vcs-Browser URL

  [ Nicolas Braud-Santoni ]
  * Bump Standards-Version to 3.9.8
  * Remove spurious dependencies from the binary package
    (Closes: #814488)

 -- Nicolas Braud-Santoni <nicolas@braud-santoni.eu>  Thu, 30 Jun 2016 15:59:39 +0200

go-mtpfs (0.0~git20150802.0.3ef47f9-2) unstable; urgency=medium

  * Fix FTBFS (thanks to Chris Lamb <lamby@debian.org>) (Closes: #802305)

 -- Julian Gilbey <jdg@debian.org>  Mon, 19 Oct 2015 11:42:06 +0100

go-mtpfs (0.0~git20150802.0.3ef47f9-1) unstable; urgency=medium

  * Initial release (Closes: #795307)

 -- Julian Gilbey <jdg@debian.org>  Sun, 13 Sep 2015 11:29:40 +0100
